/*******************************************************************************
 * Copyright (c) 2012 Adolfo Schneider, Lucas Kreutz
 * 
 * This code is licensed under MIT License. See the license.txt file
 * or access http://www.opensource.org/licenses/mit-license.php
 ******************************************************************************/
package br.ufrgs.inf.blogindexer.downloader;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;

public class Downloader {
	private String url;
	private String localPath;

	public Downloader(String url, String localPath) {
		this.url = url;
		this.localPath = localPath;
	}

	/**
	 * Downloads a file and put it in a file in a local path. Based on:
	 * http://www.guj.com.br/java/128487-fazer-download-de-arquivo-com-java
	 * 
	 * @throws MalformedURLException
	 * @throws FileNotFoundException
	 * @throws IOException
	 * 
	 */
	public void get() throws MalformedURLException, FileNotFoundException, IOException {
		// Creates the URL object
		URL url = new URL(this.url);

		// Open the file making the connection with the server
		InputStream remote = url.openStream();

		// Open the local file
		FileOutputStream local = new FileOutputStream(this.localPath);

		BufferedReader reader = new BufferedReader(new InputStreamReader(remote, "UTF-8"));
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(local, "UTF-8"));

		String inputLine;
		while ((inputLine = reader.readLine()) != null) {
			writer.write(inputLine);
		}

		// Close the streams
		writer.close();
		reader.close();
	}
}
