/*******************************************************************************
 * Copyright (c) 2012 Adolfo Schneider, Lucas Kreutz
 * 
 * This code is licensed under MIT License. See the license.txt file
 * or access http://www.opensource.org/licenses/mit-license.php
 ******************************************************************************/
package br.ufrgs.inf.blogindexer.compression;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

/**
 * Based on http://rosettacode.org/wiki/Huffman_coding#Java,
 * refined to run with all UTF-8 chars, persist the frequency table and
 * the compacted data stream (not binary, just a conceptual persistence)
 */
public class HuffmanCode {
	private HuffmanTree tree;
	private Map<Character, Integer> frequencies = new HashMap<Character, Integer>();
	private Map<Character, String> dictionary = new HashMap<Character, String>();
	private String filepath;

	public HuffmanCode(String filepath) {
		this.filepath = filepath;
	}

	public void calculateFrequencies() throws FileNotFoundException, IOException {
		FileInputStream fis;
		fis = new FileInputStream(filepath);

		BufferedInputStream buffReader = new BufferedInputStream(fis);
		DataInputStream data = new DataInputStream(buffReader);
		byte[] b = new byte[fis.available()];
		data.read(b);

		for (char c : new String(b).toCharArray()) {
			if (frequencies.containsKey(c)) {
				int oldVal = frequencies.get(c);
				frequencies.put(c, oldVal + 1);
			} else {
				frequencies.put(c, 1);
			}
		}

		fis.close();
	}

	// input is an array of frequencies, indexed by character code
	public void buildTree() {
		PriorityQueue<HuffmanTree> trees = new PriorityQueue<HuffmanTree>();
		// initially, we have a forest of leaves
		// one for each non-empty character
		for (Character character : frequencies.keySet())
			trees.offer(new HuffmanLeaf(frequencies.get(character), character));

		assert trees.size() > 0;
		// loop until there is only one tree left
		while (trees.size() > 1) {
			// two trees with least frequency
			HuffmanTree a = trees.poll();
			HuffmanTree b = trees.poll();

			// put into new node and re-insert into queue
			trees.offer(new HuffmanNode(a, b));
		}
		tree = trees.poll();
	}

	public void compress() throws FileNotFoundException, IOException {
		String tmpFilename = filepath + ".huff";

		// saves the frequencies
		serializeFrequencies();

		generateDictionary(tree, new StringBuffer());

		FileInputStream fis = new FileInputStream(filepath);

		BufferedInputStream reader = new BufferedInputStream(fis);
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(tmpFilename), "UTF-8"));

		DataInputStream data = new DataInputStream(reader);
		byte[] b = new byte[fis.available()];
		data.read(b);

		String buffer = "";
		for (char c : new String(b).toCharArray()) {
			writer.write(dictionary.get(c));
		}

		// remaining bits if the bit stream size was not multiple of 8
		if (buffer.length() > 0) {
			writer.write(Integer.parseInt(buffer, 2));
		}

		writer.close();
		reader.close();

		File oldFile = new File(filepath);
		if (oldFile.delete()) {
			File newFile = new File(tmpFilename);
			newFile.renameTo(oldFile);
		}
	}

	private void serializeFrequencies() throws IOException {
		FileOutputStream freqFile = new FileOutputStream(filepath + ".freq");
		ObjectOutputStream out = new ObjectOutputStream(freqFile);
		out.writeObject(frequencies);
		out.flush();
		out.close();
	}

	private void unserializeFrequencies() throws IOException, ClassNotFoundException, ClassCastException {
		File path = new File(filepath + ".freq");
		if (path.exists()) {
			FileInputStream indexFile = new FileInputStream(filepath + ".freq");
			ObjectInputStream in = new ObjectInputStream(indexFile);
			frequencies = (Map<Character, Integer>) in.readObject();
			in.close();
		}
	}

	public String uncompress() throws FileNotFoundException, IOException, ClassNotFoundException, ClassCastException {
		String encoded = "";

		// loads the frequencies
		unserializeFrequencies();

		// build the tree
		buildTree();

		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(filepath), "UTF-8"));

		String inputLine;
		while ((inputLine = reader.readLine()) != null) {
			encoded = encoded + inputLine;
		}

		reader.close();

		// ------------------
		HuffmanTree aux = tree;
		String text = "";
		boolean readEverything = false;

		while (!readEverything) {
			if (aux instanceof HuffmanLeaf) {
				HuffmanLeaf leaf = (HuffmanLeaf) aux;
				text = text + String.valueOf(leaf.value);

				if (encoded.length() == 0) {
					readEverything = true;
				} else {
					aux = tree;
				}
			} else if (aux instanceof HuffmanNode) {
				if (encoded.length() > 0) {
					HuffmanNode node = (HuffmanNode) aux;

					Character bit = encoded.charAt(0);
					encoded = encoded.substring(1, encoded.length());

					if (bit == '0') {
						aux = node.left;
					} else if (bit == '1') {
						aux = node.right;
					}
				} else {
					readEverything = true;
				}
			}
		}

		reader.close();

		return text;
	}

	private void generateDictionary(HuffmanTree tree, StringBuffer prefix) {
		assert tree != null;
		if (tree instanceof HuffmanLeaf) {
			HuffmanLeaf leaf = (HuffmanLeaf) tree;

			dictionary.put(leaf.value, prefix.toString());

		} else if (tree instanceof HuffmanNode) {
			HuffmanNode node = (HuffmanNode) tree;

			// traverse left
			prefix.append('0');
			generateDictionary(node.left, prefix);
			prefix.deleteCharAt(prefix.length() - 1);

			// traverse right
			prefix.append('1');
			generateDictionary(node.right, prefix);
			prefix.deleteCharAt(prefix.length() - 1);
		}
	}
}
