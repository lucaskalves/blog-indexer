/*******************************************************************************
 * Copyright (c) 2012 Adolfo Schneider, Lucas Kreutz
 * 
 * This code is licensed under MIT License. See the license.txt file
 * or access http://www.opensource.org/licenses/mit-license.php
 ******************************************************************************/
package br.ufrgs.inf.blogindexer.access;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import br.ufrgs.inf.blogindexer.config.Context;

/**
 * A sequential file indexing the pages
 */
public class SequentialIndex {
	private ArrayList<IndexRegister> registers = new ArrayList<IndexRegister>();
	private String indexFilePath = "";
	private char way = 'a';

	public SequentialIndex() {

	}

	public SequentialIndex(String indexName) {
		this.indexFilePath = Context.indexesDir + indexName;
	}

	public ArrayList<IndexRegister> getRegisters() {
		return registers;
	}

	public void setWay(char way) throws Exception {
		if (way == 'a') {
			this.way = 'a';
		} else if (way == 'd') {
			this.way = 'd';
		} else {
			throw new Exception("Invalid way.");
		}
	}

	/**
	 * Checks if a key is in the index
	 * 
	 * @param key
	 * @return true if the key exists
	 */
	public boolean contains(String key) {
		return getKeyPosition(key) > 0;
	}

	/**
	 * Returns the position of a key
	 * 
	 * @param key
	 * @return the number of the index or -1 if it's not inside the index
	 */
	private int getKeyPosition(String key) {
		int left = 0;
		int right = registers.size() - 1;
		int middle;

		// binary search
		while (left <= right) {
			middle = (left + right) / 2;

			if (way == 'a') {
				if (registers.get(middle).getKey().compareTo(key) < 0)
					left = middle + 1;
				else if (registers.get(middle).getKey().compareTo(key) > 0)
					right = middle - 1;
				else
					return middle;
			} else {
				if (registers.get(middle).getKey().compareTo(key) > 0)
					left = middle + 1;
				else if (registers.get(middle).getKey().compareTo(key) < 0)
					right = middle - 1;
				else
					return middle;
			}
		}

		return -1;
	}

	/**
	 * Get the index register by the key
	 * 
	 * @param key
	 * @return
	 * @throws Exception
	 */
	public IndexRegister getByKey(String key) throws Exception {
		int position = getKeyPosition(key);

		if (position < 0) {
			throw new Exception("The key doesn't exists.");
		}

		return registers.get(position);
	}

	/**
	 * Adds a register to the index ordered by key
	 * 
	 * @param newRegister
	 */
	public synchronized void add(IndexRegister newRegister) {
		registers.add(newRegister);
		this.sort(0, registers.size());
	}

	public void sort(int begin, int end) {
		int middle;

		if (begin < end) {
			middle = partition(begin, end);
			sort(begin, middle);
			sort(middle + 1, end);
		}
	}

	private int partition(int begin, int end) {
		int top, i;
		IndexRegister pivot = registers.get(begin);
		top = begin;
		boolean condition;

		for (i = begin + 1; i < end; i++) {
			if (way == 'a') {
				condition = registers.get(i).compareTo(pivot) < 0;
			} else {
				condition = registers.get(i).compareTo(pivot) > 0;
			}

			if (condition) {
				registers.set(top, registers.get(i));
				registers.set(i, registers.get(top + 1));
				top++;
			}
		}
		registers.set(top, pivot);
		return top;
	}

	/**
	 * Saves the index to the file
	 * 
	 * @throws IOException
	 */
	public synchronized void save() throws IOException {
		FileOutputStream indexFile = new FileOutputStream(indexFilePath);
		ObjectOutputStream out = new ObjectOutputStream(indexFile);
		out.writeObject(registers);
		out.flush();
		out.close();
	}

	/**
	 * Loads the index file
	 * 
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public void load() throws IOException, ClassNotFoundException, ClassCastException {
		File path = new File(indexFilePath);
		if (path.exists()) {
			FileInputStream indexFile = new FileInputStream(indexFilePath);
			ObjectInputStream in = new ObjectInputStream(indexFile);
			registers = (ArrayList<IndexRegister>) in.readObject();
			in.close();
		}
	}
}
