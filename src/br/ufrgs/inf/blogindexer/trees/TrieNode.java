/*******************************************************************************
 * Copyright (c) 2012 Adolfo Schneider, Lucas Kreutz
 * 
 * This code is licensed under MIT License. See the license.txt file
 * or access http://www.opensource.org/licenses/mit-license.php
 ******************************************************************************/
package br.ufrgs.inf.blogindexer.trees;

import java.io.Serializable;
import java.util.Set;
import java.util.TreeSet;

public class TrieNode implements Comparable<TrieNode>, Serializable {
	private static final long serialVersionUID = 1L;
	private String content = "";
	private Set<TrieNode> children = new TreeSet<TrieNode>();
	private Set<String> files = new TreeSet<String>();
	private boolean marked = false;

	public TrieNode() {
	}

	public TrieNode(String content) {
		this.content = content;
	}

	public String getContent() {
		return content;
	}

	public Set<TrieNode> getChildren() {
		return children;
	}

	public Set<String> getFiles() {
		return files;
	}

	/**
	 * Adds a child in the children list
	 * 
	 * @param newNode
	 */
	public void addChild(TrieNode newNode) {
		this.children.add(newNode);
	}

	/**
	 * Adds a file to the files list
	 * 
	 * @param newFile
	 */
	public void addFile(String newFile) {
		this.marked = true;
		this.files.add(newFile);
	}

	/**
	 * Checks if the current node has files associated
	 * 
	 * @return true if this node has a file associated or false if not
	 */
	public boolean hasFiles() {
		return files.size() > 0;
	}

	public boolean isWord() {
		return marked;
	}

	public int compareTo(TrieNode other) {
		return content.compareToIgnoreCase(other.getContent());
	}
}
