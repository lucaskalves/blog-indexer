/*******************************************************************************
 * Copyright (c) 2012 Adolfo Schneider, Lucas Kreutz
 * 
 * This code is licensed under MIT License. See the license.txt file
 * or access http://www.opensource.org/licenses/mit-license.php
 ******************************************************************************/
package br.ufrgs.inf.blogindexer.ui;

import java.awt.EventQueue;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.UIManager;

import br.ufrgs.inf.blogindexer.config.DataCleaner;

public class BlogIndexer {

	private JFrame frmBlogindexer;
	JMenuItem mntmAddFeeds;
	AddFeedsForm addFeedsForm;
	ListingByAuthorTable listingByAuthorTable;
	ListingByCategoryTable listingByCategoryTable;
	ListingByCommentsTable listingByCommentsTable;
	ListingByDateTable listingByDateTable;
	ListingByTitleTable listingByTitleTable;
	SearchByAuthorForm searchByAuthorForm;
	SearchByCategoriesForm searchByCategoriesForm;
	SearchByDateForm searchByDateForm;
	SearchByKeywordForm searchByKeywordForm;
	SearchByTitleForm searchByTitleForm;
	JMenuItem mntmByAuthor_1;
	JMenuItem mntmByCategories_1;
	JMenuItem mntmBycomments;
	JMenuItem mntmByDatehour_1;
	JMenuItem mntmByTitle_1;
	JMenuItem mntmByAuthor;
	JMenuItem mntmByCategories;
	JMenuItem mntmByDatehour;
	JMenuItem mntmByKeywords;
	JMenuItem mntmByTitle;
	JDesktopPane desktopPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Throwable e) {
			e.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					BlogIndexer window = new BlogIndexer();
					window.frmBlogindexer.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public BlogIndexer() {
		initialize();
		createEvents();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmBlogindexer = new JFrame();
		frmBlogindexer.setTitle("Blog Indexer");
		frmBlogindexer.setBounds(100, 100, 743, 491);
		frmBlogindexer.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JScrollPane scrollPane = new JScrollPane();
		GroupLayout groupLayout = new GroupLayout(frmBlogindexer.getContentPane());
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.TRAILING).addComponent(scrollPane, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 692, Short.MAX_VALUE));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 423, Short.MAX_VALUE).addGap(0)));

		desktopPane = new JDesktopPane();
		scrollPane.setViewportView(desktopPane);
		desktopPane.setBackground(SystemColor.inactiveCaption);
		GroupLayout gl_desktopPane = new GroupLayout(desktopPane);
		gl_desktopPane.setHorizontalGroup(gl_desktopPane.createParallelGroup(Alignment.LEADING).addGap(0, 692, Short.MAX_VALUE));
		gl_desktopPane.setVerticalGroup(gl_desktopPane.createParallelGroup(Alignment.LEADING).addGap(0, 410, Short.MAX_VALUE));
		desktopPane.setLayout(gl_desktopPane);
		frmBlogindexer.getContentPane().setLayout(groupLayout);

		JMenuBar menuBar = new JMenuBar();
		frmBlogindexer.setJMenuBar(menuBar);

		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);

		mntmAddFeeds = new JMenuItem("Add Feeds");

		mnFile.add(mntmAddFeeds);

		JMenuItem mntmClearData = new JMenuItem("Clear Data");
		mntmClearData.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				DataCleaner dc = new DataCleaner();
				dc.removeAllData();
			}
		});
		mnFile.add(mntmClearData);

		JMenuItem mntmExit = new JMenuItem("Exit");
		mntmExit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		mnFile.add(mntmExit);

		JMenu mnSearch = new JMenu("Search");
		menuBar.add(mnSearch);

		mntmByAuthor = new JMenuItem("By Author");
		mnSearch.add(mntmByAuthor);

		mntmByCategories = new JMenuItem("By Categories");
		mnSearch.add(mntmByCategories);

		mntmByDatehour = new JMenuItem("By Date/Hour");
		mnSearch.add(mntmByDatehour);

		mntmByKeywords = new JMenuItem("By Keywords");
		mnSearch.add(mntmByKeywords);

		mntmByTitle = new JMenuItem("By Title");
		mnSearch.add(mntmByTitle);

		JMenu mnListings = new JMenu("Listings");
		menuBar.add(mnListings);

		mntmByAuthor_1 = new JMenuItem("By Author");
		mnListings.add(mntmByAuthor_1);

		mntmByCategories_1 = new JMenuItem("By Categories");
		mnListings.add(mntmByCategories_1);

		mntmBycomments = new JMenuItem("By #Comments");
		mnListings.add(mntmBycomments);

		mntmByDatehour_1 = new JMenuItem("By Date/Hour");
		mnListings.add(mntmByDatehour_1);

		mntmByTitle_1 = new JMenuItem("By Title");
		mnListings.add(mntmByTitle_1);

		JMenu mnAbout = new JMenu("About");
		menuBar.add(mnAbout);

		JMenuItem mntmBlogIndexer = new JMenuItem("Blog Indexer");
		mntmBlogIndexer.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				About frame = new About();
				frame.setVisible(true);
			}
		});
		mnAbout.add(mntmBlogIndexer);
	}

	private void createEvents() {
		mntmAddFeeds.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (addFeedsForm == null || addFeedsForm.isClosed()) {
					addFeedsForm = new AddFeedsForm();
					desktopPane.add(addFeedsForm);
					addFeedsForm.show();
				}
			}
		});

		mntmByAuthor_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (listingByAuthorTable == null || listingByAuthorTable.isClosed()) {
					listingByAuthorTable = new ListingByAuthorTable();
					desktopPane.add(listingByAuthorTable);
					listingByAuthorTable.show();
				}
			}
		});

		mntmByCategories_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (listingByCategoryTable == null || listingByCategoryTable.isClosed()) {
					listingByCategoryTable = new ListingByCategoryTable();
					desktopPane.add(listingByCategoryTable);
					listingByCategoryTable.show();
				}
			}
		});

		mntmBycomments.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (listingByCommentsTable == null || listingByCommentsTable.isClosed()) {
					listingByCommentsTable = new ListingByCommentsTable();
					desktopPane.add(listingByCommentsTable);
					listingByCommentsTable.show();
				}
			}
		});

		mntmByDatehour_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (listingByDateTable == null || listingByDateTable.isClosed()) {
					listingByDateTable = new ListingByDateTable();
					desktopPane.add(listingByDateTable);
					listingByDateTable.show();
				}
			}
		});

		mntmByTitle_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (listingByTitleTable == null || listingByTitleTable.isClosed()) {
					listingByTitleTable = new ListingByTitleTable();
					desktopPane.add(listingByTitleTable);
					listingByTitleTable.show();
				}
			}
		});

		mntmByAuthor.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (searchByAuthorForm == null || searchByAuthorForm.isClosed()) {
					searchByAuthorForm = new SearchByAuthorForm();
					desktopPane.add(searchByAuthorForm);
					searchByAuthorForm.show();
				}
			}
		});

		mntmByCategories.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (searchByCategoriesForm == null || searchByCategoriesForm.isClosed()) {
					searchByCategoriesForm = new SearchByCategoriesForm();
					desktopPane.add(searchByCategoriesForm);
					searchByCategoriesForm.show();
				}
			}
		});

		mntmByDatehour.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (searchByDateForm == null || searchByDateForm.isClosed()) {
					searchByDateForm = new SearchByDateForm();
					desktopPane.add(searchByDateForm);
					searchByDateForm.show();
				}
			}
		});

		mntmByKeywords.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (searchByKeywordForm == null || searchByKeywordForm.isClosed()) {
					searchByKeywordForm = new SearchByKeywordForm();
					desktopPane.add(searchByKeywordForm);
					searchByKeywordForm.show();
				}
			}
		});

		mntmByTitle.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (searchByTitleForm == null || searchByTitleForm.isClosed()) {
					searchByTitleForm = new SearchByTitleForm();
					desktopPane.add(searchByTitleForm);
					searchByTitleForm.show();
				}
			}
		});

	}
}
