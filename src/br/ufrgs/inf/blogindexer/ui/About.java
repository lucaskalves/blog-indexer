/*******************************************************************************
 * Copyright (c) 2012 Adolfo Schneider, Lucas Kreutz
 * 
 * This code is licensed under MIT License. See the license.txt file
 * or access http://www.opensource.org/licenses/mit-license.php
 ******************************************************************************/
package br.ufrgs.inf.blogindexer.ui;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;

public class About extends JFrame {

	private static final long serialVersionUID = -2638474210397653389L;
	private JPanel contentPane;

	/**
	 * Create the frame.
	 */
	public About() {
		setTitle("About");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 149);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		JTextPane txtpnAuthors = new JTextPane();
		txtpnAuthors.setText("Blog Indexer 2012\r\nAuthors:\r\n - Adolfo Henrique Shneider\r\n - Lucas José Kreutz Alves\r\n\r\nInstituto de Informática\r\nUniversidade Federal do Rio Grande do Sul");
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING).addComponent(txtpnAuthors, GroupLayout.DEFAULT_SIZE, 424, Short.MAX_VALUE));
		gl_contentPane.setVerticalGroup(gl_contentPane.createParallelGroup(Alignment.LEADING).addComponent(txtpnAuthors, GroupLayout.PREFERRED_SIZE, 101, Short.MAX_VALUE));
		contentPane.setLayout(gl_contentPane);
	}
}
