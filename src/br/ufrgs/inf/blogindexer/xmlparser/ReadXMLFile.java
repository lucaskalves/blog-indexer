/*******************************************************************************
 * Copyright (c) 2012 Adolfo Schneider, Lucas Kreutz
 * 
 * This code is licensed under MIT License. See the license.txt file
 * or access http://www.opensource.org/licenses/mit-license.php
 ******************************************************************************/
package br.ufrgs.inf.blogindexer.xmlparser;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import br.ufrgs.inf.blogindexer.access.IndexRegister;
import br.ufrgs.inf.blogindexer.access.SequentialIndex;
import br.ufrgs.inf.blogindexer.config.Context;
import br.ufrgs.inf.blogindexer.trees.TrieTree;

public class ReadXMLFile {

	public void parse() {

		try {
			// Reads the index and get a feed file
			SequentialIndex feedsIndex = new SequentialIndex("feeds.ind");
			feedsIndex.load();
			String fileName;
			int insertedPosts = 0;
			int postCounter;

			// Creates the posts indexes
			SequentialIndex titlesIndex = new SequentialIndex("titles.ind");
			SequentialIndex authorsIndex = new SequentialIndex("authors.ind");
			SequentialIndex commentsIndex = new SequentialIndex("comments.ind");
			SequentialIndex tagsIndex = new SequentialIndex("tags.ind");
			SequentialIndex dateIndex = new SequentialIndex("date.ind");
			SequentialIndex titlesIndexDesc = new SequentialIndex("titlesDesc.ind");
			titlesIndexDesc.setWay('d');
			SequentialIndex authorsIndexDesc = new SequentialIndex("authorsDesc.ind");
			authorsIndexDesc.setWay('d');
			SequentialIndex commentsIndexDesc = new SequentialIndex("commentsDesc.ind");
			commentsIndexDesc.setWay('d');
			SequentialIndex tagsIndexDesc = new SequentialIndex("tagsDesc.ind");
			tagsIndexDesc.setWay('d');
			SequentialIndex dateIndexDesc = new SequentialIndex("dateDesc.ind");
			dateIndexDesc.setWay('d');

			// Creates the Trie-Tree for storing the contents
			TrieTree contentsTree = new TrieTree("contents.tree");

			for (IndexRegister indexRegister : feedsIndex.getRegisters()) {

				fileName = indexRegister.getAddress();
				String page = Context.feedsDir + fileName;
				System.out.println(page);

				File fXmlFile = new File(page);
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				Document doc = dBuilder.parse(fXmlFile);
				doc.getDocumentElement().normalize();

				System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
				NodeList nList = doc.getElementsByTagName("item");
				System.out.println("-----------------------");

				for (postCounter = 0; postCounter < nList.getLength(); postCounter++) {

					String postFileName = "post-" + (postCounter + insertedPosts) + ".json";

					JSONObject json = new JSONObject();
					JSONArray categories = new JSONArray();

					Node nNode = nList.item(postCounter);
					if (nNode.getNodeType() == Node.ELEMENT_NODE) {

						Element eElement = (Element) nNode;

						json.put("title", getTagValue("title", eElement));

						NodeList nList1 = doc.getElementsByTagName("channel");
						Node nNode1 = nList1.item(0);
						Element eElement1 = (Element) nNode1;
						json.put("author", getTagValue("title", eElement1));

						json.put("date", getTagValue("pubDate", eElement));

						NodeList nList0 = eElement.getElementsByTagName("category");
						for (int i = 0; i < nList0.getLength(); i++) {
							String category = nList0.item(i).getChildNodes().item(0).getNodeValue();
							categories.put(category);
							tagsIndex.add(new IndexRegister(category, postFileName));
							tagsIndexDesc.add(new IndexRegister(category, postFileName));
						}
						json.put("categories", categories);

						json.put("comments", getTagValue("slash:comments", eElement));

						String html = getTagValue("content:encoded", eElement);
						String nohtml = html.toString().replaceAll("\\<.*?>", "");
						json.put("content", nohtml);
						String link = getTagValue("link", eElement);
						json.put("originalURL", link);

						String output = json.toString();

						BufferedWriter out = new BufferedWriter(new FileWriter(Context.postsDir + postFileName));
						out.write(output);
						out.close();

						// Adds the indexes
						titlesIndex.add(new IndexRegister(getTagValue("title", eElement), postFileName));
						titlesIndexDesc.add(new IndexRegister(getTagValue("title", eElement), postFileName));
						authorsIndex.add(new IndexRegister(getTagValue("title", eElement1), postFileName));
						authorsIndexDesc.add(new IndexRegister(getTagValue("title", eElement1), postFileName));
						commentsIndex.add(new IndexRegister(getTagValue("slash:comments", eElement), postFileName));
						commentsIndexDesc.add(new IndexRegister(getTagValue("slash:comments", eElement), postFileName));

						// Sun, 01 Jul 2012 21:10:36 +000
						SimpleDateFormat dateParser = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z", Locale.US);
						Date pubDate = dateParser.parse(getTagValue("pubDate", eElement));
						dateIndex.add(new IndexRegister(Long.toString(pubDate.getTime()), postFileName));
						dateIndexDesc.add(new IndexRegister(Long.toString(pubDate.getTime()), postFileName));

						// Add the words of the content into the tree
						for (String word : nohtml.split("\\s+")) {
							contentsTree.addWord(word.toLowerCase(), postFileName);
						}

					}
				}

				insertedPosts = insertedPosts + postCounter;
			}

			// saves the indexes and the trees
			titlesIndex.save();
			authorsIndex.save();
			commentsIndex.save();
			tagsIndex.save();
			dateIndex.save();

			titlesIndexDesc.save();
			authorsIndexDesc.save();
			commentsIndexDesc.save();
			tagsIndexDesc.save();
			dateIndexDesc.save();

			contentsTree.save();

		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("Parsed the feeds and created the indexes/trees");

	}

	private static String getTagValue(String sTag, Element eElement) {
		NodeList nlList = eElement.getElementsByTagName(sTag).item(0).getChildNodes();

		Node nValue = nlList.item(0);

		return nValue.getNodeValue();
	}

}
