/*******************************************************************************
 * Copyright (c) 2012 Adolfo Schneider, Lucas Kreutz
 * 
 * This code is licensed under MIT License. See the license.txt file
 * or access http://www.opensource.org/licenses/mit-license.php
 ******************************************************************************/
package br.ufrgs.inf.blogindexer.config;

public class Context {
	public static final String fileSeparator = System.getProperty("file.separator");
	public static final String baseDir = System.getProperty("user.dir") + fileSeparator;
	public static final String resourcesDir = baseDir + "resources" + fileSeparator;
	public static final String indexesDir = resourcesDir + "indexes" + fileSeparator;
	public static final String feedsDir = resourcesDir + "feeds" + fileSeparator;
	public static final String postsDir = resourcesDir + "posts" + fileSeparator;
	public static final String treesDir = resourcesDir + "trees" + fileSeparator;
}
