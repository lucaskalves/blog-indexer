/*******************************************************************************
 * Copyright (c) 2012 Adolfo Schneider, Lucas Kreutz
 * 
 * This code is licensed under MIT License. See the license.txt file
 * or access http://www.opensource.org/licenses/mit-license.php
 ******************************************************************************/
package br.ufrgs.inf.blogindexer.config;

import java.io.File;

public class DataCleaner {
	public void removeAllData() {
		removeFeeds();
		removeIndexes();
		removePosts();
		removeTrees();
	}

	public void removeIndexes() {
		File folder = new File(Context.indexesDir);
		File[] listOfFiles = folder.listFiles();

		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].getName().endsWith(".ind")) {
				listOfFiles[i].delete();
			}
		}
	}

	public void removeTrees() {
		File folder = new File(Context.treesDir);
		File[] listOfFiles = folder.listFiles();

		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].getName().endsWith(".tree")) {
				listOfFiles[i].delete();
			}
		}
	}

	public void removeFeeds() {
		File folder = new File(Context.feedsDir);
		File[] listOfFiles = folder.listFiles();

		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].getName().endsWith(".xml")) {
				listOfFiles[i].delete();
			}
		}
	}

	public void removePosts() {
		File folder = new File(Context.postsDir);
		File[] listOfFiles = folder.listFiles();

		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].getName().endsWith(".json") || listOfFiles[i].getName().endsWith(".freq") || listOfFiles[i].getName().endsWith(".huff")) {
				listOfFiles[i].delete();
			}
		}
	}
}
